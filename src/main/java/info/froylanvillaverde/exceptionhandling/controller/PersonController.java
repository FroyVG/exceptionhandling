package info.froylanvillaverde.exceptionhandling.controller;

import info.froylanvillaverde.exceptionhandling.dto.ServiceException;
import info.froylanvillaverde.exceptionhandling.entities.Person;
import info.froylanvillaverde.exceptionhandling.service.PersonService;
import info.froylanvillaverde.exceptionhandling.utils.PersonRequestValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonRequestValidationUtil validationUtil;

    @GetMapping(value = "/person-management/person/{personId}")
    public ResponseEntity<Person> getPerson(@PathVariable("personId") Long personId)
            throws ServiceException {

        validationUtil.verifyPersonId(personId);
        Person person = personService.getPersonByPersonId(personId);
        return new ResponseEntity<>(person, HttpStatus.OK);

    }

    @GetMapping(value = "/person-management/{ssn}")
    public ResponseEntity<Person> getPersonBySsn(@PathVariable("ssn") long ssn) throws ServiceException {

        validationUtil.verifyPersonSsn(ssn);
        Person person = personService.getPersonBySsn(ssn);
        return new ResponseEntity<>(person, HttpStatus.OK);

    }

    @PostMapping("/person-management/person")
    public ResponseEntity<Person> createPerson(@RequestBody Person person) {

        personService.createPerson(person);
        return new ResponseEntity<>(person, HttpStatus.CREATED);

    }
}
