package info.froylanvillaverde.exceptionhandling.utils;

import info.froylanvillaverde.exceptionhandling.apierror.ErrorCode;
import info.froylanvillaverde.exceptionhandling.dto.ServiceException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class PersonRequestValidationUtil {
	
	
		public void verifyPersonId(Long personId) throws ServiceException {
			
			 if (!(personId instanceof Long) || personId.equals("undefined") || personId.equals("{personId}")) {
				 throw new ServiceException(ErrorCode.SERVICE_001, new Object[] {personId});
			 }
		 }
		
		public void verifyPersonSsn(Long ssn) throws ServiceException{
			 if ( !(ssn instanceof Long) || ssn==0 || ssn ==null) {
				 throw new ServiceException(ErrorCode.SERVICE_001, new Object[] {ssn});
			 }
		 }
		
	}


