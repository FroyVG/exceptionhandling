package info.froylanvillaverde.exceptionhandling.service;

import info.froylanvillaverde.exceptionhandling.dto.ServiceException;
import info.froylanvillaverde.exceptionhandling.entities.Person;
import info.froylanvillaverde.exceptionhandling.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class PersonService implements IPersonService{

    @Autowired
    private PersonRepository personRepository;

    public Person getPersonByPersonId(Long personId){
        Person person = null;

        person = personRepository.findById(personId).get();

        return person;
    }

    public Person createPerson(Person person) throws ServiceException {
        log.info("person created: {}", Objects.toString(person));
        return personRepository.save(person);
    }

    public Person getPersonBySsn(long ssn) {
        Person person = null;
        person = personRepository.getPersonBySsn(ssn);

        return person;
    }
}
