package info.froylanvillaverde.exceptionhandling.service;

import info.froylanvillaverde.exceptionhandling.dto.ServiceException;
import info.froylanvillaverde.exceptionhandling.entities.Person;

public interface IPersonService {

    public Person createPerson(Person person)throws ServiceException;

    public Person getPersonByPersonId(Long personId) throws ServiceException;

    public Person getPersonBySsn(long ssn);
}
