package info.froylanvillaverde.exceptionhandling.repository;

import info.froylanvillaverde.exceptionhandling.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	public Person getPersonBySsn(Long ssn);

}
