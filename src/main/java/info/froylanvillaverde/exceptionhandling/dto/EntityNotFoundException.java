package info.froylanvillaverde.exceptionhandling.dto;

public class EntityNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public EntityNotFoundException() {
        super();
    }
}
